from ubuntu:20.10

RUN apt-get update && \
	apt-get install -y zip unzip curl tar git-core p7zip-full wget lcov build-essential cmake \
		python3 python3-pip clang clang-tidy clang-format luarocks valgrind \
		libluajit-5.1-dev libx11-dev libxrandr-dev libxi-dev libudev-dev libgl1-mesa-dev

RUN luarocks install luacheck
RUN luarocks install busted
RUN update-alternatives --install /usr/bin/python python /usr/bin/python3 1
RUN update-alternatives --install /usr/bin/pip pip /usr/bin/pip3 1
RUN pip install PyTexturePacker
